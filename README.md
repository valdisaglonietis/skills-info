
# Skills Information

Some information relevant to skills competition

## Database engine - InnoDB

If you have problems with migrations where some foreign keys or indexes are too long, you can change database engine to **InnoDB** that supports longer foreign keys or indexes.

### config/database.php

Add **'engine'** variable to **mysql** database connection inside **config/database.php**

```
    ...
    'connections' => [
        ...
        'mysql' => [
            ...
            // Setting Database Engine
            'engine' => env('DB_ENGINE', 'innodb'),
        ],
        ...
    ],
    ...
```


### .env

Add DB_ENGINE inside **.env**

```
...
DB_ENGINE=innodb
...
```

## PHPStorm possible issues
If you have **PHPStorm** version that is older than **2020.3.EAP** 
you might experience issues with PHPStorm and have a permanent screen from PHPStorm

```
"loading components"
```

In that case contact an expert, if it is an issue to create a remote project manually
